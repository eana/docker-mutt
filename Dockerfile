FROM debian:stable

RUN set -xe && \
	mkdir -vp /home/user && \
    useradd -d /home/user -G users -u 1000 user && \
    chown -R user:users /home/user && \
    apt-get update && \
    apt-get -y dist-upgrade && \
    apt-get -y install --no-install-recommends ca-certificates w3m mutt vim notmuch-mutt msmtp wget man less curl git && \
    mkdir -p /home/user/.mutt/certificates && \
    wget -c 'http://mxr.mozilla.org/mozilla/source/security/nss/lib/ckfw/builtins/certdata.txt?raw=1' -O /home/user/.mutt/certificates/cacert.pem && \
    apt-get -y --purge autoremove wget && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ADD --chown=user:users bin/generate_configs /usr/local/bin/generate_configs
ADD --chown=user:users templates/ /home/user/

ENV BROWSER="w3m" EDITOR="vim" HOME="/home/user" LANG="C.UTF-8"

USER user

CMD ["/usr/local/bin/generate_configs", "mutt", "-F", "~/.muttrc"]
