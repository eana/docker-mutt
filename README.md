# Run mutt inside docker

Run your favorite mail client [mutt](http://www.mutt.org) in a
[Docker](http://docker.io) container without installing
[mutt](http://www.mutt.org) on your host system.

Without [Jessie
Frazelle](https://github.com/jessfraz/dockerfiles/tree/master/mutt) work, this
project wouldn't be possible.

## Sending and retrieving emails
To be able to send and receive mail we need a MTA (mail transfer agent). We can
have both features in a full MTA like exim, but again, it's hard to configure
and more feature rich than what we really need. For retrieving you can use
[offlineimap](https://gitlab.com/eana/docker-offlineimap). For sending emails
we will use [msmtp](https://marlam.de/msmtp/).

## Password
You need to set up an "App password" for `msmpt`. To do this, log into
gmail, click your user in the top right corner. Click Manage your Google
Account. Click Security. Click App passwords. Then generate one for
`msmtp`.

If you like to keep your dotfiles on github or some other publicly available
place, you might not like having your password just written out. Some people
get fancy and integrate with keyrings and such, but for this little project we
will template the password in `.msmtprc` and replace it at startup.

To do so we will export the generated code (without spaces) to the `PASSWORD`
environment variable.

```bash
 export PASSWORD="gmail_app_password"
```

From the `msmtp` manual: _Accounts defined in the user configuration file
override accounts from the system configuration file. The user configuration
file must have no more permissions than user read/write_

```bash
chmod 600 templates/.msmtprc
```

## How to build it

```
# From the msmtp manual: Accounts defined in the user configuration file
# override accounts from the system configuration file. The user configuration
# file must have no more permissions than user read/write.
chmod 600 templates/.msmtprc
docker build -t mutt .
```

## How to start it

```
export LOCAL_USER=$(eval echo ~$USER)
export MUTT_PATH="/path/where/mutt/will/store/its/files/"
export MUTT_FIRSTNAME="Firstname"
export MUTT_LASTNAME="Lastname"
export MUTT_USERNAME="username"
export MUTT_DOMAIN="gmail.com"
export MUTT_EMAIL="${MUTT_USERNAME}@${MUTT_DOMAIN}"
export OFFLINEIMAP="${HOME}/.offlineimap"
export MAILDIR="${OFFLINEIMAP}/${MUTT_EMAIL}/Mail"

mkdir -vp ${MUTT_PATH}/${MUTT_EMAIL}
mkdir -vp ${MUTT_PATH}/${MUTT_EMAIL}/cache
mkdir -vp ${MUTT_PATH}/${MUTT_EMAIL}/attachments

touch ${MUTT_PATH}/${MUTT_EMAIL}/aliases

docker run --name mutt --rm -it -e FIRSTNAME="${MUTT_FIRSTNAME}" \
                                -e LASTNAME="${MUTT_LASTNAME}" \
                                -e USERNAME="${MUTT_USERNAME}" \
                                -e DOMAIN="${MUTT_DOMAIN}" \
                                -e MAILDIR="${MAILDIR}" \
                                -e TERM=xterm-256color \
                                -e PASSWORD="${MUTT_PASSWORD}" \
                                -v /etc/localtime:/etc/localtime:ro \
                                -v ${MAILDIR}:/home/user/Mail \
                                -v ${MUTT_PATH}/${MUTT_EMAIL}/aliases:/home/user/.mutt/aliases \
                                -v ${MUTT_PATH}/${MUTT_EMAIL}/cache:/home/user/.mutt/cache \
                                -v ${MUTT_PATH}/${MUTT_EMAIL}/cache:/home/user/.cache \
                                -v ${MUTT_PATH}/${MUTT_EMAIL}/attachments:/home/user/attachments \
                                mutt
```

##  Populate the aliases file

```
docker run --name goobook -v ${MUTT_PATH}/aliases:/aliases -it debian:unstable bash
apt-get install -y --no-install-recommends nodejs goobook vim
goobook config-template > ~/.goobookrc
goobook authenticate --noauth_local_webserver
goobook dump_contacts | egrep "fullName|email address" | sed 's/.*<ns1:fullName>/Full_Name: /;s/.*<ns1:email address="/Email: /;s/<\/ns1:fullName>//;s/".*//g' > /aliases.txt
vim /aliases.js

var fs = require('fs')
var contents = fs.readFileSync('aliases.txt').toString()
var r = /Full_Name: ([\w|,@. ]+)\nEmail: ([-\w@.]+)/gm
var replaced1 = contents.replace(r, 'alias $1 <$2>')
var r2 = /^Email: ([\w@.]+)$/gm
var replaced2 = replaced1.replace(r2, 'alias $1 <$1>')
console.log(replaced2)

nodejs /aliases.js > /aliases
```

_Disclaimer:_
 1. When I am writing this I am under pressure with some other stuff and I
  don't have time for something more elegant.
 2. [goobook](https://gitlab.com/goobook/goobook) is searching only on _My
  Contacs_ group, which for me is not acceptable
