#!/bin/bash

file="${HOME}/.mutt/scripts/bofh/bofh-excuses.txt"
line=$(shuf -i 1-465 -n 1)
excuse=$(sed "$line q;d" $file)
echo "+ FIRSTNAME"
echo "+ BOFH excuse #$line"
echo "+ $excuse"
echo ""
