#!/bin/sh
# http://wcm1.web.rice.edu/mutt-tips.html

MESSAGE=$(cat)

NEWALIAS=$(echo "${MESSAGE}" | sed -n '/^From: /,/^Subject: /p' | sed -e '$ d' -e "s/[\,\"\']//g" -e 's/^From: //g; s/^To: //g; s/^Cc: //g; s/^Bcc: //; s/^ *//g' | awk '{print "alias", tolower($(NF-1))"-"tolower($2)" " $0;}' | sort | uniq | sort)

if grep -Fxq "${NEWALIAS}" "${HOME}"/.mutt/aliases; then
    :
else
    echo "${NEWALIAS}" >> "${HOME}"/.mutt/aliases
fi

echo "${MESSAGE}"
