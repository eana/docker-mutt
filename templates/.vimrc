syntax on

set background=dark

if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" word wrap
function ToggleWrap()
 if (&wrap == 1)
   set nowrap
   set linebreak
 else
   set wrap
   set linebreak
   set tw=80
   set formatoptions+=a
 endif
endfunction

:map <F3> :call ToggleWrap()<CR>
:map! <F3> ^[:call ToggleWrap()<CR>

" spell checking
:map <F2> :setlocal spell! spelllang=en_us<CR>

autocmd Filetype mail setlocal fo+=aw textwidth=72

" use spaces instead on tabs
set tabstop=4 shiftwidth=4 expandtab
:map <F4> :retab<CR>

" mark the 80th column
set colorcolumn=80
highlight ColorColumn ctermbg=darkgray guibg=darkgray

" disable mouse integration
set mouse-=a

" add full file path to your existing statusline
set statusline+=%F
set laststatus=2

" a more useful statusline
set statusline +=%1*\ %n\ %*            "buffer number
set statusline +=%2*%m%*                "modified flag
set statusline +=%1*%=%5l%*             "current line
set statusline +=%2*/%L%*               "total lines
set statusline +=%1*%4v\ %*             "virtual column number
set statusline +=%2*0x%04B\ %*          "character under cursor

" remove all trailing whitespace
nnoremap <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>
